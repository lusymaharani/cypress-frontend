import carPage from '../support/page-object/carPage';
import CarPage from '../support/page-object/carPage';

describe('Rent Test', () => {
  
    it('Successfully add new car', () => {
        CarPage.visit();
        CarPage.clickmenuCar();
        CarPage.clickaddCar();
        CarPage.addphoto1('https://cdn.motor1.com/images/mgl/mLRqo/s1/4x3/vector-w8.webp');
        CarPage.addphoto2('https://hips.hearstapps.com/hmg-prod/images/1991-vector-w8-twin-turbo-104-64835824d44b0.jpg');
        CarPage.addBrand('BMW');
        CarPage.addModel('2022');
        CarPage.addGearbox('automatic');
        CarPage.addFueltype('petrol');
        CarPage.addPrice('700');
        CarPage.addAvailble(1);
        CarPage.clickCreatebutton();

        
    });

  });
  
