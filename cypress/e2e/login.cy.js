import LoginPage from '../support/page-object/loginPage';

describe('Login Test', () => {
    it('Unsuccessfully login', () => {
      // Tindakan untuk kredensial yang salah
      LoginPage.visit();
      LoginPage.fillEmail('lusy@gmail.com');
      LoginPage.fillPassword('Inikatasandi');
      LoginPage.clickLoginButton();
  
      // Verifikasi bahwa URL saat ini adalah halaman login (karena login gagal)
      cy.url().should('eq', 'http://localhost:3000/login');
  
      // Verifikasi pesan error hanya untuk kredensial yang salah
      cy.get('#toast-1').should('be.visible');
      cy.get('#toast-1-description').should('contain', 'Wrong password try again.');
    });
  
    it('Successfully login with valid credentials', () => {
      // Tindakan untuk kredensial yang salah
      LoginPage.visit();
      LoginPage.fillEmail('lusy@gmail.com');
      LoginPage.fillPassword('Testing123');
      LoginPage.clickLoginButton();
   
      // Verifikasi pesan berhasil
      cy.get('#toast-1').should('be.visible');
      cy.get('#toast-1-description').should('contain', "You've logged in successfully.");

      // Verifikasi bahwa URL saat ini adalah halaman appointment (karena login berhasil)
      cy.url().should('eq', 'http://localhost:3000/cars');
    });
  });
  