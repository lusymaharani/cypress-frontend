import LoginPage from '../support/page-object/loginPage';
import RentPage from '../support/page-object/rentPage';

describe('Rent Test', () => {
    beforeEach(() => {
        LoginPage.visit();
        LoginPage.fillEmail('lusy@gmail.com');
        LoginPage.fillPassword('Testing123');
        LoginPage.clickLoginButton();

        cy.get('#toast-1').should('be.visible');
        cy.get('#toast-1-description').should('contain', "You've logged in successfully.");
  
        cy.url().should('eq', 'http://localhost:3000/cars');
    });
  
    it('Successfully rent', () => {
        RentPage.searchCar();
        RentPage.clickCar();
        RentPage.selectRentalDate('2024-01-13');
        RentPage.selectReturnDate('2024-01-31');
        cy.wait(3000);
        RentPage.clickRentButton();

        cy.get('#toast-2').should('be.visible');
        cy.get('#toast-2-description').should('contain', 'Rent created successfully');
        
        cy.url().should('eq', 'http://localhost:3000/cars');
    });

  });
  
