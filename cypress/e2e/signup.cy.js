import SignupPage from '../support/page-object/signupPage';

describe('Signup Test', () => {
    it('Unsuccessfully signup because validation telephone', () => {

      // Tindakan untuk kredensial yang salah
      SignupPage.visit();
      SignupPage.fillFirstname('Lusy');
      SignupPage.fillLastname('Maharani');
      SignupPage.fillTelephone('086325124098');
      SignupPage.fillEmail('lusy@testing.com');
      SignupPage.fillPassword('Initesting123');
      SignupPage.clickSignupButton();
  
      // Verifikasi bahwa URL saat ini adalah halaman signup (karena gagal signup)
      cy.url().should('eq', 'http://localhost:3000/signup');
  
      // Verifikasi pesan error hanya untuk kredensial yang salah
      cy.get('#toast-1').should('be.visible');
      cy.get('#toast-1-description').should('contain', 'The telephone must be at least 20 characters.');
    });

    it('Unsuccessfully signup because validation password', () => {

        // Tindakan untuk kredensial yang salah
        SignupPage.visit();
        SignupPage.fillFirstname('Lusy');
        SignupPage.fillLastname('Maharani');
        SignupPage.fillTelephone('086325124351');
        SignupPage.fillEmail('lusy@testing.com');
        SignupPage.fillPassword('Initest');
        SignupPage.clickSignupButton();
    
        // Verifikasi bahwa URL saat ini adalah halaman signup (karena gagal signup)
        cy.url().should('eq', 'http://localhost:3000/signup');
    
        // Verifikasi pesan berhasil
        cy.get('#toast-1').should('be.visible');
        cy.get('#toast-1-description').should('contain', 'Password must be minimum 8 characters, at least 1 letter and 1 number.');
      });
     
    it('Successfully signup', () => {
        // Tindakan untuk kredensial yang benar
        SignupPage.visit();
        SignupPage.fillFirstname('Lusy');
        SignupPage.fillLastname('Maharani');
        SignupPage.fillTelephone('08632512435103123431');
        SignupPage.fillEmailRandom();
        SignupPage.fillPassword('Initesting123');
        SignupPage.clickSignupButton();
  
        // Verifikasi pesan error hanya untuk kredensial yang salah
        cy.get('#toast-1').should('be.visible');
        cy.get('#toast-1-description').should('contain', 'Account created successfully');
        
        // Verifikasi bahwa URL saat ini adalah halaman appointment (karena login berhasil)
        cy.url().should('eq', 'http://localhost:3000/login');
    });
  });
  