class RentPage {
    visit() {
      cy.visit('http://localhost:3000/login');
    }
    
    searchCar() {
        cy.contains('Dacia Logan');
    }
    
    clickCar() {
        cy.get(':nth-child(4) > .vehicle-card > .details > .css-h94677 > .chakra-button').click();
    }

    selectRentalDate(date) {
        cy.get('.css-15esdq8 > :nth-child(3)').type(date);
    }
  
    selectReturnDate(date) {
        cy.get('.css-15esdq8 > :nth-child(5)').type(date);
    }

    clickRentButton() {
        cy.get('.css-15esdq8 > .chakra-button').click();
    }
 }
  
  export default new RentPage;
  