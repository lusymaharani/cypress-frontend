class SignupPage {
    visit() {
      cy.visit('http://localhost:3000/signup');
    }
  
    fillFirstname(firstname) {
        cy.get('#Firstname').type(firstname);
    }
 
    fillLastname(lastname) {
        cy.get('#Lastname').type(lastname);
    }
  
    fillTelephone(telephone) {
        cy.get('#Telephone').type(telephone);
    }
 
    fillEmail(email) {
        cy.get('#Email').type(email);
    }
  
    fillEmailRandom() {
        cy.get('#Email').type(`${Date.now()}@lusytest.com`);
    }

    fillPassword(password) {
      cy.get('#Password').type(password);
    }
  
    clickSignupButton() {
        cy.get('.btn-secondary').click();
    }
}
  
  export default new SignupPage;
  