class CarPage {
    visit() {
      cy.visit('http://localhost:3000/dashboard');
    }

    clickmenuCar() {
      cy.contains('Cars').click();
    }
  
    clickaddCar() {
      cy.get('.css-1u5e79q > .chakra-button').click();
    }
    
    addphoto1(photo1) {
      cy.get('#photo1').type(photo1);
    }
     
    addphoto2(photo2) {
      cy.get('#photo2').type(photo2);

    }
         
    addBrand(brand) {
      cy.get('#brand').type(brand);

    }     

    addModel(model) {
      cy.get('#model').type(model);

    }     

    addGearbox(gearbox) {
      cy.get('#gearbox').type(gearbox);

    }     

    addFueltype(fueltype) {
      cy.get('#fuel_type').type(fueltype);

    }     

    addPrice(price) {
      cy.get('#price').type(price);

    }     

    addAvailble(available) {
      cy.get('#available').type(available);

    }

    clickCreatebutton() {
      cy.get('.css-8ha015').click();
    }
  }
  
  export default new CarPage;
  