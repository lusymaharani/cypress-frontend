class LoginPage {
    visit() {
      cy.visit('http://localhost:3000/login');
    }
  
    fillEmail(email) {
      cy.get('#Email').type(email);
    }
  
    fillPassword(password) {
      cy.get('#Password').type(password);
    }
  
    clickLoginButton() {
        cy.get('.btn-primary').click();
    }
}
  
  export default new LoginPage;
  